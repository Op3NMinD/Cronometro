﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Cronometro
{
    public partial class Form1 : Form
    {

        Cronometro cronometro = new Cronometro();        
        
        public Form1()
        {
            InitializeComponent();                     
        } 

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            LabelTempo.Text = cronometro.start();    
                 
        }

        private void ButtonStop_Click(object sender, EventArgs e)
        {           
            cronometro.stop();
            LabelTempo.Text = cronometro.elapsed().ToString();
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            LabelTempo.Text = cronometro.reset();
           
        }

        
    }
}
